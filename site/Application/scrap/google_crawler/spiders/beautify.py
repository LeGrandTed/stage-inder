import simplejson as json

obj = None
with open('data.json', encoding='utf8') as f:
    obj = json.load(f)

outfile = open('data.json', "w", encoding='utf8')
outfile.write(json.dumps(obj, indent=2, sort_keys=True, ensure_ascii=False))
outfile.close()