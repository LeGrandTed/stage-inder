import scrapy 
import json
from bs4 import BeautifulSoup
from urllib.request import urlopen
import simplejson as json
from .listes_entreprises import return_liste_links
import scrapy.exceptions

class SpiderWiki(scrapy.Spider): 
  name = "wiki"

  start_urls = [ 
    "https://fr.wikipedia.org/wiki/Thales",
    "https://fr.wikipedia.org/wiki/Cdiscount",
    "https://fr.wikipedia.org/wiki/Amazon",
    "https://fr.wikipedia.org/wiki/Microsoft",
    "https://fr.wikipedia.org/wiki/IBM",
    "https://fr.wikipedia.org/wiki/LGM_Group",
    "https://fr.wikipedia.org/wiki/Tesla_(automobile)",
    "https://fr.wikipedia.org/wiki/Apple",
    "https://fr.wikipedia.org/wiki/Xiaomi",
    "https://fr.wikipedia.org/wiki/Airbus_Commercial_Aircraft"
   ]

  def parse(self, response):
    page = self.start_urls[0]

    json_file = open("dataWiki.json", "a", encoding='utf8')
    json_file.close()
    
    entreprise = response.css('h1.firstHeading::text').get()
    
    html = urlopen(page)
    
    p1 = response.css('p').getall()
    p = p1[4]
    res = BeautifulSoup(p, 'html.parser')
    description = res.get_text()
    soup = BeautifulSoup(html, 'html.parser')


    liste_scrap = ["Création", "Fondateurs", "Forme juridique", "Slogan", "Effectif", "Site web", "Chiffre d'affaires", "Résultat net"]
    liste_info = list()
    tables = soup.find_all('tr')
    for th in tables:
      info = BeautifulSoup(th.get_text(), 'html.parser')
      if info != "":
        liste_info.append(info.get_text())
    liste_scrap_fin = list()
    for info in liste_info:
      for elem in liste_scrap:
        if elem in info:
          elem_scrap = info.replace("\n", ", ")
          liste_scrap_fin.append(elem_scrap)

    elem_scrap_str = ""
    for elem in liste_scrap_fin:
      elem_scrap_str += elem

    liste_split = elem_scrap_str.split(", ")
    liste_index_pop = list()
    for i in range(len(liste_split)):
      if liste_split[i]=="":
        liste_index_pop.append(i)
      if liste_split[i] in liste_scrap:
        liste_index_pop.append(i)
    liste_index_pop.reverse()
    for indice in liste_index_pop:
      liste_split.pop(indice)


    dico = {
      'Entreprise':entreprise,
      'Description':description,
    }
    

    json_string = json.dumps(dico, indent=2, ensure_ascii=False)
    json_file = open("dataWiki.json", "a")
    
    json_file.write(json_string)
    json_file.close()


class FinaleSpider(scrapy.Spider): 
  name = "data"

  liste = return_liste_links()
  start_urls = liste

  custom_settings = {
    'DEFAULT_REQUEST_HEADERS': {'accept': '*/*'},
    'USER_AGENT': 'some user-agent value',
  }

  def parse_page(self, response):
    if 'Bandwidth exceeded' in response.body:
        raise scrapy.exceptions.CloseSpider('bandwidth_exceeded')

  def parse(self, response):
    # print(len(entreprises))
    for link in self.start_urls:
      entreprises = json.load(open('data.json'))
      if len(entreprises) >= 73:
        raise scrapy.exceptions.CloseSpider()
      else:
        page = link
        html = urlopen(page)

        soup = BeautifulSoup(html, 'html.parser')
        mydivs = soup.find_all("div", {"class": "col-sm-12"})

        dico_scrap = {
          " Siret" : "0",
          " Code APE" : "NA",
          " Forme juridique" : "NA",
          " Statut" : "NA",
          " Dép. siège social" : "NA",
          " Adresse" : "NA",
          " Site" : "NA",
          " Téléphone" : "NA",
          " Effectif" : "0",
          " Activités et savoir-faire détaillés" : "NA",
          " Effectif total de l'entreprise" : "0",
          " CA" : "NA",
          " Lien Image" : "https://dynamic.brandcrowd.com/asset/logo/1e81db4b-5cce-43b3-8cf5-dcf5b43f7584/logo-search-grid-1x?v=637344647818000000&text="
        }

        liste_info = list()
        for div in mydivs:
          info = BeautifulSoup(div.get_text(), 'html.parser')
          if info != "":
            infotext = info.get_text()
            if " : " in infotext:
              infosplit = infotext.split(" : ")
              if infosplit[0] in dico_scrap.keys():
                dico_scrap[infosplit[0]] = infosplit[1]
            else:
              liste_info.append(info.get_text())

        dico_scrap[" Lien Image"] += liste_info[0]
        
        if liste_info[0] != "":
          dico = {
            liste_info[0]: [
              dico_scrap
            ],
          }
        

        if liste_info[0] not in entreprises:
          with open("data.json", "r+", encoding='utf8') as file:
            data = json.load(file)
            loading = (len(data)*100)/73
            print(round(loading), "%")
            data.update(dico)
            file.seek(0)
            json.dump(data, file, ensure_ascii=False)

