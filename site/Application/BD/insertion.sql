insert into ENT values (0,"A");
insert into ENT values (1,"B");
insert into ENT values (2,"C");
insert into ENT values (3,"D");
insert into ENT values (4,"E");
insert into ENT values (5,"F");
insert into ENT values (6,"G");


insert into USER values (1,"HENRIETTE", "Jules","01/01/2001", 0);
insert into USER values (2,"TUMAR", "Berfan","02/01/2001", 0);
insert into USER values (3,"FAUBOURG", "Luca","03/01/2001", 1);
insert into USER values (4,"LANGLAIS", "Alexis","04/01/2001", 0);
insert into USER values (5,"RIGNON", "Bastien","05/01/2001", 1);
insert into USER values (6,"BOILLOT", "Damien","06/01/2001", 0);

insert into COMPETENCE values ("PHP");
insert into COMPETENCE values ("SQL");
insert into COMPETENCE values ("HTML");
insert into COMPETENCE values ("CSS");
insert into COMPETENCE values ("Python");
insert into COMPETENCE values ("C");
insert into COMPETENCE values ("Java");


insert into EST_COMPETENT values (1, "PHP");
insert into EST_COMPETENT values (6, "PHP");
insert into EST_COMPETENT values (2, "HTML");
insert into EST_COMPETENT values (4, "HTML");
insert into EST_COMPETENT values (2, "CSS");
insert into EST_COMPETENT values (4, "CSS");
insert into EST_COMPETENT values (5, "SQL");
insert into EST_COMPETENT values (3, "C");
insert into EST_COMPETENT values (3, "Java");


insert into EXPLOITE_COMPETENCEs values (0, "PHP");
insert into EXPLOITE_COMPETENCEs values (1, "PHP");
insert into EXPLOITE_COMPETENCEs values (2, "HTML");
insert into EXPLOITE_COMPETENCEs values (3, "HTML");
insert into EXPLOITE_COMPETENCEs values (1, "HTML");
insert into EXPLOITE_COMPETENCEs values (2, "CSS");
insert into EXPLOITE_COMPETENCEs values (3, "CSS");
insert into EXPLOITE_COMPETENCEs values (6, "SQL");
insert into EXPLOITE_COMPETENCEs values (1, "C");
insert into EXPLOITE_COMPETENCEs values (2, "Java");
