from ast import Pass
from calendar import Calendar
from imp import PY_SOURCE
from os import name, stat
from re import M
from tokenize import String
from flask_wtf import FlaskForm
from sqlalchemy import false, null
from wtforms import StringField, HiddenField, PasswordField, DateField, validators, BooleanField, TextAreaField
from wtforms.fields.numeric import IntegerField
from wtforms.validators import DataRequired, Email
from .app import app, loginURL, registerURL, homeURL, db, profileURL
from .BDclass import *
from flask import jsonify, render_template, request, redirect, url_for, flash, session
from flask_login import login_user, current_user, logout_user
from hashlib import sha256
from functools import wraps
import json
# from flask import g 


# from werkzeug.security import generate_password_hash

class RegisterForm(FlaskForm):
    etu_num = IntegerField("Numéro étudiant", validators = [validators.DataRequired(message = "Veuillez remplir ce champ.")])
    name = StringField("Nom", validators = [validators.DataRequired(message = "Veuillez remplir ce champ.")])
    first_name = StringField("Prénom", validators = [validators.DataRequired(message = "Veuillez remplir ce champ.")])
    mail = StringField("Adresse mail", validators = [validators.DataRequired(message = "Veuillez remplir ce champ.")])
    password = PasswordField("Mot de passe", [
        validators.DataRequired(message = "Veuillez remplir ce champ."),
        validators.EqualTo("confirm", message = "Mot de passe incorrect")
    ])
    confirm = PasswordField("Confirmer mot de passe",
                            validators = [validators.DataRequired(message = "Veuillez remplir ce champ.")])
    birthday = DateField('Start Date', format = '%m/%d/%Y')
    admin = BooleanField("Administrateur")

    def user_register(self):
        m = sha256()
        m.update(self.password.data.encode())
        password = m.hexdigest()
        u = create_user(self.etu_num.data, self.name.data, self.first_name.data, self.admin.data, password, self.birthday.data)
        print(u)


class LoginForm(FlaskForm):
    etu_num = IntegerField("Numéro étudiant", validators = [validators.DataRequired(message = "Veuillez remplir ce champ.")])
    password = PasswordField("Mot de passe", validators = [validators.DataRequired("Veuillez remplir ce champ.")])

    def get_authenticated_user(self):
        user = get_User(self.etu_num.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        password = m.hexdigest()
        return user if password == user.password else None

class CompRegisterForm(FlaskForm):
    nom_comp = StringField(" Nom de la compétence")

    def comp_register(self):
        add_Cpt(self.nom_comp.data)

    def editModif(self,id):
        edit_cpt(id,self.nom_comp.data)

    def formEditComp(self,comp):
        self.nom_comp.data=comp.competence


class EntRegisterForm(FlaskForm):
    validator = [validators.DataRequired(message = "Veuillez remplir ce champ.")]
    name = StringField("Nom", validators = validator)
    # priorite = BooleanField("Prioritaire")
    black_List = BooleanField("BlackList")
    desc = TextAreaField("Description")
    code_ape = StringField("Code APE", validators = validator)
    effectifs = IntegerField("Effectifs")
    num_siren = IntegerField("Numéro SIREN", validators = validator)
    num_siret = IntegerField("Numéro SIRET", validators = validator)
    telephone = StringField("Téléphone")
    adresse = StringField("Adresse")
    

    def ent_register(self):
        create_ent(self.name.data, False, self.black_List.data, self.desc.data, self.code_ape.data, 
            self.effectifs.data, self.num_siret.data, self.num_siret.data,self.telephone.data, self.adresse.data)
    
    def ent_modif(self,idEnt):
        edit_ent(idEnt, self.name.data, self.desc.data, self.code_ape.data, 
            self.effectifs.data, self.num_siret.data)

    def formEditEnt(self,company):
        self.name.data=company.nomEnt
        self.desc.data=company.desc
        self.code_ape.data=company.code_ape
        self.effectifs.data=company.effectifs
        self.num_siret.data=company.num_siret
        self.telephone.data=company.telephone
        self.adresse.data=company.adresse
        

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre de vérifier que l'utilisateur actuel est bien connecté
#----------------------------------------------------------------------------------------------------------------------------------

def login_required(f):
    """
    Function which will make it possible to check that the current user is well connected

    @param f:
    @type f:
    @return:
    @rtype:
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not current_user.is_authenticated:
            flash("vous devez vous connecter")
            return redirect(url_for('login', next = request.url))
        return f(*args, **kwargs)
    return decorated_function

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre de vérifier que l'utilisateur actuel est administrateur ou non
#----------------------------------------------------------------------------------------------------------------------------------

def admin_required(f):
    """
    Function that will allow you to verify that the current user is an administrator or not

    @param f:
    @type f:
    @return:
    @rtype:
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not current_user.is_authenticated:
            return redirect(url_for('login', next = request.url))
        elif not current_user.administrateur:
            return redirect(url_for('notFound', next = request.url))
        return f(*args, **kwargs)
    return decorated_function

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre d'afficher le template d'acceuil et va différer en fonction 
# de si l'utilisateur est oui ou non administrateur
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/")
def homePage():
    """
    Function that will display the welcome template and will differ depending on
    whether or not the user is an administrator

    @return:
    @rtype:
    """
    if request.method == 'POST':
        return None
    else:
        if not current_user.is_authenticated:
            return render_template("home.html", login = loginURL, register = registerURL, home = homeURL,
            title = "Accueil", isLogged = current_user.is_authenticated)
        else:
            return render_template("home.html", login = loginURL, register = registerURL, home = homeURL,
            title = "Accueil", isLogged = current_user.is_authenticated, isAdmin = current_user.administrateur)

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre d'afficher le template d'inscription pour pouvoir créer
# son compte 
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Inscription", methods = ['GET', 'POST'])
def register():
    """
    Function that will display the registration template to be able to create
    its account

    @return:register's template or redirect to login
    @rtype:
    """
    f = RegisterForm()
    if request.method == 'POST':
        f.user_register()
        return redirect(url_for('login'))
    if f.validate_on_submit():
        print("Inscription")
        return redirect(url_for('login'))
    if not current_user.is_authenticated:
        return render_template("register.html", login = loginURL, register = registerURL, home = homeURL, title = "Inscription",
            form = f, profile = profileURL, isLogged = current_user.is_authenticated)
    else:
        return render_template("register.html", login = loginURL, register = registerURL, home = homeURL, title = "Inscription",
            form = f, profile = profileURL, isLogged = current_user.is_authenticated, isAdmin = current_user.administrateur)

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va vous permettre d'afficher le template de connection, de gérer
# la validité de la connection et de rediriger en fonction de si l'on est 
# un administrateur ou non
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Connexion", methods = ['GET', 'POST'])
def login():
    """
    Function that will allow you to display the connection template, manage
    the validity of the connection and redirect depending on whether one is
    an administrator or not

    @return: login's template if not authentificated or redirect to stages
    @rtype:
    """
    user = None
    f = LoginForm()
    if f.validate_on_submit():
        user = f.get_authenticated_user()
    if user:
        login_user(user)
        flash("Connexion réussi avec succès !")
        return redirect(url_for('stages'))
    if not current_user.is_authenticated:
        return render_template("login.html", form = f, title = "Connexion", isLogged = current_user.is_authenticated)
    else :
        return render_template("login.html", form = f, title = "Connexion", isLogged = current_user.is_authenticated, 
            isAdmin = current_user.administrateur)

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre de se déconnecter de son compte à va rediriger vers 
# la page d'acceuil
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Déconnexion", methods = ['GET','POST'])
@login_required
def logout():
    """
    Function that will allow you to disconnect from your account to be redirected to
    the home page

    @return: redirect to homePage
    @rtype:
    """
    logout_user()
    return redirect(url_for('homePage'))

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre d'afficher un template de contact en fonction de si 
# l'on est administrateur ou non
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Contact", methods = ['GET', 'POST'])
def contact():
    """
    Function that will display a contact template depending on whether
    whether you are an administrator or not

    @return: contact's template
    @rtype:
    """
    if not current_user.is_authenticated:
        return render_template("contact.html", title = "Contact", isLogged = current_user.is_authenticated)
    else :
        return render_template("contact.html", title = "Contact", isLogged = current_user.is_authenticated, 
            isAdmin = current_user.administrateur)

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre d'afficher un template de gestion de ses paramètres en fonction
# de si l'utilisateur est administrateur ou non 
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Settings", methods = ['GET', 'POST'])
def settings():
    """
    Function that will display a template for managing its parameters based on
    whether the user is an administrator or not

    @return: setting's template
    @rtype:
    """
    if not current_user.is_authenticated:
        return render_template("settings.html", title = "Paramètres", isLogged = current_user.is_authenticated)
    else :
        return render_template("settings.html", title = "Paramètres", isLogged = current_user.is_authenticated, 
            isAdmin = current_user.administrateur)

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre d'afficher un template de l'historique des entreprises
# proposée à l'utilisateur connecté
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Historique", methods = ['GET', 'POST'])
@login_required
def historique():
    """
    Function that will allow you to display a template of the history of companies
    proposed to the connected user

    @return: history's template
    @rtype:
    """
    page = request.args.get('page', 1, type=int)
    entreprises = Ent.query.paginate(page=page, per_page=10, error_out=True)
    like_comp = get_like(current_user.numEtu)
    dislike_comp = get_dislike(current_user.numEtu)
    print("-------------------- TOUTES ENTREPRISES :", get_status_company(current_user.numEtu))
    print("-------------------- ENTREPRISE LIKE :", like_comp)
    print("-------------------- ENTREPRISE DISLIKE :", dislike_comp)
    return render_template("history.html", title = "Historique", isLogged = current_user.is_authenticated, isAdmin = current_user.administrateur,liked_companies = like_comp, disliked_companies = dislike_comp)

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va afficher seulement aux administrateur, la template principale des administrateur
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Admin", methods = ['GET', 'POST'])
@admin_required
def admin():
    """
    Function that will display only to administrators, the main template of administrators

    @return: admin's template
    @rtype:
    """
    ents = get_all_ent()
    users = get_all_user()
    cpts = get_all_comp()
    avgCptEtu = get_avg_comp_by_etu()
    avgDisLike = get_avg_dislike()
    avgLike = get_avg_like()
    return render_template("admin.html", title = "Admin", isLogged = current_user.is_authenticated, isAdmin = current_user.administrateur, 
        ents = ents, users = users, cpts = cpts, avgCptEtu = avgCptEtu, avgDisLike = avgDisLike, avgLike = avgLike)

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre d'afficher un template pour les administrateurs pour créer 
# un nouvel utilisateur dans la base de donnée
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Admin/NewUser", methods = ['GET', 'POST'])
@admin_required
def newUser():
    """
    Function that will display a template for administrators to create a new user in 
    the database

    @return: newUser's template
    @rtype:
    """
    f = RegisterForm()
    if request.method == 'POST':
        f.user_register()
        return redirect(url_for('admin'))
    # if f.validate_on_submit():
    #     print("Inscription")
    #     return redirect(url_for('login'))
    return render_template("newUser.html", login = loginURL, register = registerURL, home = homeURL, title = "Ajout utilisateur",
        form = f, profile = profileURL, isLogged = current_user.is_authenticated, isAdmin = current_user.administrateur)

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre d'afficher un template pour les administrateurs pour créer 
# une nouvelle entreprise dans la base de donnée
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Admin/NewEnt", methods = ['GET', 'POST'])
@admin_required
def newEnt():
    """
    Function that will display a template for administrators to create a new company in 
    the database

    @return:newEnt's template
    @rtype:
    """
    f = EntRegisterForm()
    if request.method == 'POST':
        f.ent_register()
        return redirect(url_for('admin'))
    # if f.validate_on_submit():
    #     print("Inscription")
    #     return redirect(url_for('login'))
    return render_template("newEnt.html", login = loginURL, register = registerURL, home = homeURL, title = "Ajout entreprise",
        form = f, profile = profileURL, isLogged = current_user.is_authenticated, isAdmin = current_user.administrateur)


@app.route("/Admin/Ent/<int:id>", methods=['GET', 'POST'])
@admin_required
def UpdateEnt(id):
    f = EntRegisterForm()
    ent=get_Ent(id)
    # print(ent.idEnt)
    if request.method == 'POST':
        f.ent_modif(id)
        return redirect(url_for('admin'))
    # if f.validate_on_submit():
    #     print("Inscription")
    #     return redirect(url_for('login'))
    f.formEditEnt(ent)
    return render_template("adminEnt.html", login=loginURL, register=registerURL, home=homeURL, title="Modifier entreprise",
        form=f, ent=ent, profile=profileURL,isLogged=current_user.is_authenticated,isAdmin=current_user.administrateur)


#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre d'afficher un template pour les administrateurs pour créer 
# une nouvelle compétence dans la base de donnée
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Admin/NewComp", methods = ['GET', 'POST'])
@admin_required
def newComp():
    """
    Function that will display a template for administrators to create a new skill in 
    the database

    @return:newComp's template
    @rtype:
    """
    f = CompRegisterForm()
    if request.method == 'POST':
        f.comp_register()
        return redirect(url_for('admin'))
    # if f.validate_on_submit():
    #     print("Inscription")
    #     return redirect(url_for('login'))
    return render_template("newComp.html", login = loginURL, register = registerURL, home = homeURL, title = "Ajout Compétence",
        form = f, profile = profileURL, isLogged = current_user.is_authenticated, isAdmin = current_user.administrateur)

@app.route("/Admin/Comp/<int:id>", methods=['GET', 'POST'])
@admin_required
def UpdateComp(id):
    f = CompRegisterForm()
    comp=get_Competence(id)
    # print(ent.idEnt)
    if request.method == 'POST':
        f.editModif(id)
        return redirect(url_for('admin'))
    # if f.validate_on_submit():
    #     print("Inscription")
    #     return redirect(url_for('login'))
    f.formEditComp(comp)
    return render_template("adminComp.html", login=loginURL, register=registerURL, home=homeURL, title="Modifier Compétence",
        form=f, comp=comp, profile=profileURL,isLogged=current_user.is_authenticated,isAdmin=current_user.administrateur)


#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre à un administrateur de supprimer une entreprise de la 
# base de donnée
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Admin/DeleteEnt/<int:id>", methods = ['POST'])
@admin_required
def delEnt(id):
    """
    Function that will allow an administrator to delete a company from the database

    @param id:id of company
    @type id:int
    @return:The statut of operation
    @rtype:str
    """
    status = del_ent(id)
    return str(status)

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre à un administrateur de supprimer un étudiant de la 
# base de donnée
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Admin/DeleteEtu/<int:id>", methods = ['POST'])
@admin_required
def delEtu(id):
    """
    Function that will allow an administrator to delete a student from the database

    @param id:id of student
    @type id:int
    @return:The statut of operation
    @rtype:str
    """
    status = del_user(id)
    return str(status)

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre à un administrateur de supprimer une compétence de la 
# base de donnée
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Admin/DeleteComp/<int:id>", methods = ['POST'])
@admin_required
def delComp(id):
    """
    Function that will allow an administrator to delete a skill from the database

    @param id:id of skill
    @type id:int
    @return:The statut of operation
    @rtype:str
    """
    status = del_cpt(id)
    return str(status)

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre de sauvegarder des modifications apportés à une entreprise
# par un administrateur
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Admin/SauverEnt", methods = ["POST"])
# @admin_required
def SauverEnt():
    """
    Function that will allow you to save changes made to a company by an administrator

    @return:
    @rtype:str
    """
    liste = json.loads(request.form.get('BlackListData'))
    # print("Liste :")
    # print(liste)
    for x in liste:
        blackList_ent(x[0], x[1])
        # print("ENT :"+x[0]+str(x[1]))
    return "0"

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre de sauvegarder des modifications apportés à un étudiant
# par un administrateur
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Admin/SauverEtu", methods = ["POST"])
# @admin_required
def SauverEtu():
    """
    Function that will allow you to save changes made to a student by an administrator

    @return:
    @rtype:str
    """
    liste = json.loads(request.form.get('AdminData'))
    # print("Liste :")
    # print(liste)
    for x in liste:
        setAdmin(x[0], x[1])
        # print("User :"+x[0]+" "+str(x[1]))
    return "0"

# @app.route("/Admin/Entreprise/<int:id>", methods=['GET', 'POST'])
# @admin_required
# def admin_ent(id):
#     ent = get_Ent(id)
#     if request.method == 'POST':
#         # f.edit_ent(idEnt, nomEnt, priorite, blackList)
#         return redirect(url_for('admin_ent',id = id))
#     return render_template("newUser.html", login=loginURL, register=registerURL, home=homeURL, title=ent.nomEnt, 
#         profile=profileURL,isLogged=current_user.is_authenticated,isAdmin=current_user.administrateur)


#----------------------------------------------------------------------------------------------------------------------------------
# Cette fonction va récupérer les compétences de l'utilisateur actuel et les envoyer au template profil.html qui va les afficher
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Profil", methods = ['GET'])
@login_required
def profil():
    """
    This function will retrieve the skills of the current user and send them to the profil.html template which will display them

    @return: profile's template
    @rtype:
    """
    utilisateur = get_User(current_user.numEtu)
    competencesDispo=get_all_comp()
    return render_template("profil.html", title = "Profil", isLogged = current_user.is_authenticated, isAdmin = current_user.administrateur, 
        nom = utilisateur.nom, prenom = utilisateur.prenom, numetu = utilisateur.numEtu, competences = utilisateur._competences, 
        competences_dispo = competencesDispo)

#----------------------------------------------------------------------------------------------------------------------------------
# Cette fonction va être appelée après avoir reçu un retour POST par profil() et va permettre de mettre à jour les
# compétences de l'utililsateur actuel dans la base de donnée, puis de rafraichir la page en rappelant profil
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Profil", methods = ['POST'])
@login_required
def profilrecup():
    """
    This function will be called after having received a POST return by profil() and will allow to update the
    skills of the current user in the database, then to refresh the page by recalling profile

    @return:Refresh template profil
    @rtype:
    """
    nouvelleDonnee=request.form
    competencesDispo=get_all_comp()
    for comp in competencesDispo:
        if comp.competence in nouvelleDonnee:
            if(comp not in get_User(current_user.numEtu)._competences):
                add_cpt_to_user(current_user.numEtu, comp.id)
        else:
            if(comp in get_User(current_user.numEtu)._competences):
                del_cpt_to_user(current_user.numEtu, comp.id)
    return profil()

#----------------------------------------------------------------------------------------------------------------------------------
# Cette fonction va utiliser la fonction "rechercheEntreprises" pour trouver 9 entreprises correspondant à l'utilisateur
# et va les envoyer dans le template sous la forme de 9 cartes
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Stages", methods = ['GET'])
@login_required
def stages():
    """
    This function will use the "rechercheEntreprises" function to find 9 companies corresponding to the user
    and will send them in the template as 9 cards

    @return: swipeComp's template
    @rtype:
    """
    entreprises = rechercheEntreprises()
    return render_template("swipeComp.html", title = "Entreprises", isLogged = current_user.is_authenticated, 
        isAdmin = current_user.administrateur, entreprises = entreprises)
        

#----------------------------------------------------------------------------------------------------------------------------------
# Cette fonction va être appelée via le JS des cartes d'entreprises, avec les données du nom de l'entreprise
# avec l'argument 'nameEnt' et la réponse de l'étudiant face à cette entreprise via l'argument 'likeEnt'
# qui vaudra 'true' ou 'false'.
# Cette fonction permettra via les donnée précisées avant de savoir la réponse de l'étudiant face à l'entreprise
# en parametre de la donnée js et de mettre à jour la BD en fonction
#----------------------------------------------------------------------------------------------------------------------------------

@app.route('/recupStages', methods = ['POST'])
def recupStages():
    """
    This function will be called via the JS of the company cards, with the company name data
    with the argument 'nameEnt' and the student's response to this company via the argument 'likeEnt'
    which will be 'true' or 'false'.
    This function will allow via the data specified before knowing the student's response to the company
    as a parameter of the js data and update the database accordingly

    @return:Nothing
    @rtype:str
    """
    nameEnt = request.form.get('nameEnt')
    likeEnt = request.form.get('likeEnt')
    idEnt = get_idEnt_by_name(nameEnt)
    numEtu = current_user.numEtu
    if(likeEnt == "true"):
        purpose_ent_to_user(numEtu, idEnt)
        set_like(numEtu, idEnt, True)
    else:
        purpose_ent_to_user(numEtu, idEnt)
        set_like(numEtu, idEnt, False)
    return ""


@app.route('/Admin/getStatsLikes',methods = ['GET'])
def getStatsLike():
    if(request.method == 'GET'):
        data = stats_likes()
        return jsonify({'data':data})
    return jsonify({'data':'ERROR GET ONLY'})


@app.route('/Admin/getStatsAPE',methods = ['GET'])
def getStatsApe():
    if(request.method == 'GET'):
        data = stats_ape()
        return jsonify({'data':data})
    return jsonify({'data':'ERROR GET ONLY'})

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre d'afficher un page erreur 404 personnalisée en cas d'erreur
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/404",methods = ['GET','POST'])
def notFound():
    """
    Function that will allow you to display a personalized 404 error page in the event of an error

    @return:404's template
    @rtype:
    """
    return render_template("404.html", title = "404", isLogged = current_user.is_authenticated, isAdmin = current_user.administrateur)

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre de calculer le nombre de points communs entre l'entreprise présenté et l'étudiant
#----------------------------------------------------------------------------------------------------------------------------------

def pointsCommunEntreprise(nomEnt):
    """
    Function that will allow you to calculate the number of common points between the company presented and the student

    @param nomEnt:Company to test common points
    @type nomEnt:Ent
    @return:number of common points
    @rtype:int
    """
    cpt = 0
    for comp in get_Ent_by_name(nomEnt)._competences:
        if comp in current_user._competences:
            cpt += 1
    return cpt

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre de dire si une entreprise est apte ou non à être présenté à cet étudiant c'est à dire si elle est
# dans la blacklist ou non si le nombre de candidat proposé est > 10 ou si à déjà été présenté à cet étudiant
#----------------------------------------------------------------------------------------------------------------------------------

def matchEntrepriseUser(nomEnt):
    """
    Function which will allow to say if a company is suitable or not to be presented to this student, i.e. if it is
    in the blacklist or not if the number of candidate proposed is > 10 or if has already been presented to this student

    @param nomEnt:A company to be checked
    @type nomEnt:Ent
    @return:if this company is compatible
    @rtype:Boolean
    """
    entreprise = get_Ent_by_name(nomEnt)
    if entreprise in current_user._entreprises or entreprise.nb_proposition > 10 or entreprise.blackList == True:
        return False
    return True

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre via l'argument passé par rechercheEntreprises qui est une liste de nom d'entreprises dont l'on
# va prendre les 9 premiers , où l'on va prendre l'objet entreprise grâce à son nom et de les ajouter à une nouvelle liste
# et utiliser pour chaque entreprise la fonction purpose_ent_to_user qui va faire en sorte que l'on ne reproposera pas l'entreprise
# à cet étudiant
#----------------------------------------------------------------------------------------------------------------------------------

def retourne9Entreprises(listeNomEntre):
    """
    Function that will allow with the argument passed by "researchCompanies" which is a list of names of companies whose
    will take the first 9 , where we will take the business object by its name and add them to a new list
    and use for each company the purpose_ent_to_user function which will ensure that the company will not be offered again
    to this student

    @param listeNomEntre:a list of companies sorted by preference of the current student
    @type listeNomEntre:list(Ent)
    @return:return a list of 9 companies
    @rtype:list(ent)
    """
    retour9entreprises = []
    for nom in listeNomEntre:
        if len(retour9entreprises) < 9:
            entreprise = get_Ent_by_name(nom)
            retour9entreprises.append(entreprise)
    return retour9entreprises

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre via les fonctions pointsCommunEntreprise / matchEntrepriseUser et retourne9Entreprise
# de retourner une liste des 9 entreprises qui convienne le mieux à l'étudiant connecté grâce à ses conptences par ordre croissant
# et de filtrer les entreprises étant dans la black list ayant + de 10 candidats de proposés et celles qui ont déjà été proposé
# à l'étudiant via la fonction matchEntrepriseUser
#----------------------------------------------------------------------------------------------------------------------------------

def rechercheEntreprises():
    """
    Function that will allow with the "pointsCommunEntreprise" / "matchEntrepriseUser" and "retourne9Entreprise" functions
    to return a list of the 9 companies that best suit to the connected student thanks to his skills in ascending order.
    This function also filter the companies being in the black list, those having more than 10 candidates proposed and those
    which have already been proposed to the student with the "matchEntrepriseUser" function.

    @return:return a list of 9 companies
    @rtype:list(Ent)
    """
    dictEntreprises = dict()  
    for ent in get_all_ent():
        if matchEntrepriseUser(ent.nomEnt):
            dictEntreprises[ent.nomEnt] = (pointsCommunEntreprise(ent.nomEnt), ent.priorite)
    res=sorted(dictEntreprises, key = lambda key : (dictEntreprises[key][0], dictEntreprises[key][1]), reverse=True)
    retour=list(reversed(retourne9Entreprises(res)))
    return retour

def stats_likes():
    """
    Function that will highlight the 4 most popular companies and one representing the rest

    @return:return a list of 4 ape number with their percentage of like
    @rtype:list(set(str,int))
    """
    APELike = stat_like_Ent()
    totLike = 0
    res = []
    for numAPE, nbLike in APELike:
        totLike += nbLike
    autre = totLike
    if totLike != 0:
        for numAPE, nbLike in APELike[0:4]:
            pourcentage = (nbLike/totLike)*100
            autre -= nbLike
            res.append((numAPE, pourcentage))
        res.append(("AUTRE", (autre/totLike)*100))
    return res

def stats_ape():
    """
    Function that will hightlight the 4 most popular APE_number and one representing the rest

    @return:return a list of 4 ape number with their percentage of companies
    @rtype:list(set(str,int))
    """
    APEEnt = stat_APE_Ent()
    totEnt = 0
    res = []
    autre = 0
    for numAPE, nbEnt in APEEnt:
        totEnt += nbEnt
    autre = totEnt
    if totEnt != 0:
        for numAPE, nbEnt in APEEnt[0:4]:
            pourcentage = (nbEnt/totEnt)*100
            autre -= nbEnt
            res.append((numAPE, pourcentage))
        res.append(("Autre", (autre/totEnt)*100))
    return res
    