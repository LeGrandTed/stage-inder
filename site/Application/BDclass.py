from itertools import count
import datetime
from flask_login import UserMixin
from sqlalchemy import select, update, func
from .app import db, login_manager

lien_cpt_etu = db.Table('cpt_etu',
                        db.Column("num_etu", db.Integer, db.ForeignKey("user.numEtu")),
                        db.Column("cmpt_id", db.Integer, db.ForeignKey("competence.id")))

lien_cpt_ent = db.Table('cpt_ent',
                        db.Column("idEnt", db.Integer, db.ForeignKey("ent.idEnt")),
                        db.Column("cmpt_id", db.Integer, db.ForeignKey("competence.id")))

proposition = db.Table('prop',
                       db.Column("num_etu", db.Integer, db.ForeignKey("user.numEtu")),
                       db.Column("idEnt", db.Integer, db.ForeignKey("ent.idEnt")),
                       db.Column("like", db.Boolean))


class Log(db.Model):
    """
    class used to track all logging to the application
    """
    __tablename__ = "log"
    idLog = db.Column(db.Integer, primary_key=True, nullable=False)
    logTime = db.Column(db.DateTime, nullable=True)


class Ent(db.Model):
    """
    class defining a compagny equivalent to a table in a database by ORM sqlalchemy
    """
    __tablename__ = "ent"
    idEnt = db.Column(db.Integer, primary_key=True, nullable=False)
    nomEnt = db.Column(db.String(80), unique=True, nullable=False)
    priorite = db.Column(db.Boolean, nullable=False)
    blackList = db.Column(db.Boolean, nullable=False)
    desc = db.Column(db.String(300))
    nb_proposition = db.Column(db.Integer, default=0)
    code_ape = db.Column(db.String(5))
    effectifs = db.Column(db.Integer)
    num_siren = db.Column(db.Integer())
    num_siret = db.Column(db.Integer())
    telephone = db.Column(db.String(30))
    adresse = db.Column(db.String(60))
    image = db.Column(db.String(300))

    _competences = db.relationship('Competence', secondary=lien_cpt_ent,
                                   backref=db.backref('lien_cpt_ent', lazy='dynamic',
                                                      )
                                   )

    users = db.relationship('User', backref='entreprises', lazy='dynamic', secondary=proposition)

    def __repr__(self):
        return '<Entreprise : %r (%s) %s>' % (self.nomEnt, self.idEnt, self._competences)


class User(db.Model, UserMixin):
    """
    class defining a user equivalent to a table in a database by ORM sqlalchemy
    """
    __tablename__ = "user"
    nom = db.Column(db.String(20), nullable=False)
    prenom = db.Column(db.String(20), nullable=False)
    password = db.Column(db.String(20), nullable=False)
    numEtu = db.Column(db.String(20), nullable=False, primary_key=True)
    dateNaissance = db.Column(db.DateTime, nullable=True)
    administrateur = db.Column(db.Boolean, nullable=False)

    _competences = db.relationship('Competence', secondary=lien_cpt_etu,
                                   backref=db.backref('lien_cpt_etu', lazy='dynamic'
                                                      )
                                   )

    _entreprises = db.relationship('Ent', secondary=proposition,
                                   backref=db.backref('proposition', lazy='dynamic'))

    def get_id(self):
        """
        this function is necessary for user authentication with flasklogin
        :return: the identifier called numEtu
        :rtype: the identifier called numEtu
        """
        return self.numEtu

    def __repr__(self):
        return '<USER : %s %s %s %s %s %s>' % (
            self.prenom, self.nom, self.password, self.numEtu, self._competences, self._entreprises)


class Competence(db.Model):
    """
    class defining a skill equivalent to a table in a database by ORM sqlalchemy
    """
    __tablename__ = "competence"
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    competence = db.Column(db.String(80), nullable=False, unique=True)
    entreprises = db.relationship('Ent', backref='competences', lazy='dynamic', secondary=lien_cpt_ent)
    users = db.relationship('User', backref='competences', lazy='dynamic', secondary=lien_cpt_etu,
                            )

    def __repr__(self):
        return '<Competence : %r>' % self.competence


def get_sample_ent_pc():
    """
    allows us to obtain a sample of 12 random companies contained in the database

    :return: a list of 12 companies
    :rtype: list
    """
    return Ent.query.limit(12).all()


def get_sample_ent_tel():
    """
    allows us to obtain a sample of 1 random companies contained in the database

    :return: 1 Ent
    :rtype: Ent
    """
    return Ent.query.limit(1).all()


def get_User(numetu):
    """
    find a User object by is numetu

    :param numetu: it's the ID of a User
    :type numetu: String
    :return: a User object
    :rtype: User
    """
    return User.query.filter(User.numEtu == numetu).first()


def get_Competence(cpt_id):
    """
    find Competence object by is id

    :param cpt_id: it's the ID of a skill (Competence class)
    :type cpt_id: int
    :return: a Competence object
    :rtype: Competence
    """
    return Competence.query.filter(Competence.id == cpt_id).first()


def get_Ent(ent_id):
    """
    find Ent object by is id

    :param ent_id: it's the ID of a compagny (Ent class)
    :type ent_id: int
    :return: an Ent object
    :rtype: Ent
    """
    return Ent.query.filter(Ent.idEnt == ent_id).first()


def get_User_by_name(nom, prenom):
    """
    find a User by its name and surname

    :param nom: User surname
    :type nom: String
    :param prenom: User name
    :type prenom: String
    :return: a User object
    :rtype: User
    """
    return db.session.query(User).filter(User.nom == nom and User.prenom == prenom).first()


def get_Ent_by_name(ent_name):
    """
    find a compagny by its name

    :param ent_name: compagny name
    :type ent_name: String
    :return: the compagny
    :rtype: Ent
    """
    return db.session.query(Ent).filter(Ent.nomEnt == ent_name).first()


def get_idEnt_by_name(ent_name):
    """
    find the ID of a compagny by its name

    :param ent_name: compagny name
    :type ent_name: String
    :return: the id of the compagny
    :rtype: int
    """
    res = db.session.query(Ent.idEnt).filter(Ent.nomEnt == ent_name).first()
    return res[0]


def get_Cpt_by_name(cpt_name):
    """
    find a skill by its name

    :param cpt_name: the name of the skill
    :type cpt_name: String
    :return: a skill
    :rtype: Competence
    """
    cpt = db.session.query(Competence).filter(Competence.competence == cpt_name).first()
    return cpt


def get_nb_proposition(id_ent):
    """
    finds the number of times a company has been proposed

    :param id_ent: the ID of the compagny
    :type id_ent: int
    :return: the number of times a company has been proposed
    :rtype: int
    """
    ent = get_Ent(id_ent)
    return ent.nb_proposition


def add_Cpt(cpt_name):
    """
    add a skill to the database

    :param cpt_name: skill name
    :type cpt_name: String
    :return: No return
    :rtype: None
    """
    cpt_name = cpt_name.upper()
    db.session.add(Competence(competence=cpt_name))
    db.session.commit()


def add_Ent(nomEnt, priorite, blackList, desc, code_ape, effectifs, num_siren, num_siret, telephone, adresse, image):
    """
    add a compagny to the database

    :param nomEnt: the name of the compagny
    :type nomEnt: String
    :param priorite: if the company has a priority
    :type priorite: boolean
    :param blackList: if the company is blacklisted
    :type blackList: boolean
    :param desc: the description of the company
    :type desc: String
    :param code_ape: APE code of the compagny
    :type code_ape: String
    :param effectifs: the workforce of the compagny
    :type effectifs: int
    :param num_siren: siren number of the compagny
    :type num_siren: String
    :param num_siret: siret number of the compagny
    :type num_siret: String
    :param telephone: telephone number of the compagny
    :type telephone: String
    :param adresse: adresse of the compagny
    :type adresse: String
    :param image: url to the compagny logo/image
    :type image: String
    :return: no return
    :rtype: None
    """
    db.session.add(
        Ent(nomEnt=nomEnt, priorite=priorite, blackList=blackList, desc=desc, code_ape=code_ape, effectifs=effectifs, 
            num_siren=num_siren, num_siret=num_siret, telephone=telephone, adresse=adresse, image=image))
    db.session.commit()


def add_User(nom, prenom, administrateur, password, dateNaissance, numEtu):
    """
    add a User to the database

    :param nom: the user surname
    :type nom: String
    :param prenom: the user name
    :type prenom: String
    :param administrateur: if the user is an administrator
    :type administrateur: boolean
    :param password: the user password
    :type password: String
    :param dateNaissance: the user's date of birth
    :type dateNaissance: DateTime
    :param numEtu: the ID of the user
    :type numEtu: String
    :return: no return
    :rtype: None
    """
    db.session.add(User(nom=nom, prenom=prenom, administrateur=administrateur, password=password,
                        dateNaissance=dateNaissance, numEtu=numEtu))
    db.session.commit()


def add_cpt_to_user(numEtu, id_cpt):
    """
    add a skill to a user in the database

    :param numEtu: the ID of a user
    :type numEtu: String
    :param id_cpt: the ID of the skill
    :type id_cpt: int
    :return: no return
    :rtype: None
    """
    u = get_User(numEtu)
    u._competences.append(get_Competence(id_cpt))
    db.session.commit()


def del_cpt_to_user(numEtu, id_cpt):
    """
    removes a skill from a user

    :param numEtu: the ID of the user
    :type numEtu: String
    :param id_cpt: the ID of the skill
    :type id_cpt: int
    :return: no return
    :rtype: None
    """
    u = get_User(numEtu)
    u._competences.remove(get_Competence(id_cpt))
    db.session.commit()


def add_cpt_to_ent(id_ent, id_cpt):
    """
    add a skill to a compagny in the database

    :param id_ent: the ID of the compagny
    :type id_ent: int
    :param id_cpt: the ID of the skill
    :type id_cpt: int
    :return: no return
    :rtype: None
    """
    e = get_Ent(id_ent)
    e._competences.append(get_Competence(id_cpt))
    db.session.commit()


def del_cpt_to_ent(id_ent, id_cpt):
    """
    removes a skill from a user

    :param id_ent: the ID of the compagny
    :type id_ent: int
    :param id_cpt: the ID of the skill
    :type id_cpt: int
    :return: no return
    :rtype: None
    """
    e = get_Ent(id_ent)
    e._competences.remove(get_Competence(id_cpt))
    db.session.commit()


def add_nb_proposition(id_ent):
    """
    increments the number of times the company identified by the ID has been proposed

    :param id_ent: the ID of the compagny
    :type id_ent: int
    :return: no return
    :rtype: None
    """
    ent = get_Ent(id_ent)
    ent.nb_proposition += 1
    db.session.commit()


def edit_ent(idEnt, nomEnt, desc,code_ape,effectifs,num_siret):
    """
    edit the information about a compagny by its ID

    :param idEnt: the ID of the compagny
    :type idEnt: int
    :param nomEnt: the name of the compagny
    :type nomEnt: String
    :param priorite: if the company has a priority
    :type priorite: boolean
    :param desc: The description of the company
    :type desc: String
    :param code_ape: the code APE of the company
    :type code_ape: String
    :param effectifs: the number of employees in the company
    :type effectifs: int
    :param num_siret: the siret number of the company
    :type num_siret: int

    :return: if the edition was successful
    :rtype: boolean
    """
    modif = Ent.query.filter_by(idEnt=idEnt).one()
    modif.nomEnt = nomEnt
    modif.desc = desc
    modif.code_ape = code_ape
    modif.effectifs = effectifs
    modif.num_siren = num_siret
    modif.num_siret = num_siret
    db.session.commit()
    return True


# def edit_user(numEtu, nom, prenom, administrateur, password, dateNaissance):
#     """
#     edit the information about a user identified by its numEtu

#     :param numEtu: the ID of a user
#     :type numEtu: String
#     :param nom: the surname of a user
#     :type nom: String
#     :param prenom: the name of a user
#     :type prenom: String
#     :param administrateur: if the user is an administrator
#     :type administrateur: boolean
#     :param password: the password of the user
#     :type password: String
#     :param dateNaissance: the user's date of birth
#     :type dateNaissance: DateTime
#     :return: if the edition was successful
#     :rtype: boolean
#     """
#     state = db.session.query(User).filter(User.numEtu == numEtu).update(
#         {Ent.nom: nom,
#          Ent.prenom: prenom,
#          Ent.administrateur: administrateur,
#          Ent.password: password,
#          Ent.dateNaissance: dateNaissance})
#     db.session.commit()
#     return state


def edit_cpt(id_cpt, cpt_name):
    """
    edit the information of a skill by its ID

    :param id_cpt: the ID of a skill
    :type id_cpt: int
    :param cpt_name: the name of a skill
    :type cpt_name: String
    :return: if the edition was successful
    :rtype: boolean
    """
    modif = Competence.query.filter_by(id=id_cpt).one()
    modif.competence = cpt_name
    db.session.commit()
    return True


def del_ent(id_ent):
    """
    removes a company from the database

    :param id_ent: the ID of a compagny
    :type id_ent: int
    :return: if the supression was successful
    :rtype: boolean
    """
    state = db.session.query(Ent).filter(Ent.idEnt == id_ent).delete(synchronize_session=False)
    db.session.commit()
    return state


def del_user(numEtu):
    """
    removes a user from the database can raise an exception if this user is a user

    :param numEtu: the ID of a user
    :type numEtu: String
    :return: if the supression was successful
    :rtype: boolean
    """
    user = User.query.filter(User.numEtu == numEtu).first()
    if not user.administrateur:
        state = db.session.query(User).filter(User.numEtu == numEtu).delete(synchronize_session=False)
        db.session.commit()
        return state
    raise Exception("This user is an administrator")


def del_cpt(id_cpt):
    """
    removes a skill from the database

    :param id_cpt: the ID of a skill
    :type id_cpt: int
    :return: if the supression was successful
    :rtype: boolean
    """
    state = db.session.query(Competence).filter(Competence.id == id_cpt).delete(synchronize_session=False)
    db.session.commit()
    return state


def create_user(numEtu, nom, prenom, administrateur, password, dateNaissance):
    """
    create a user and put it in the database

    :param nom: the user surname
    :type nom: String
    :param prenom: the user name
    :type prenom: String
    :param administrateur: if the user is an administrator
    :type administrateur: boolean
    :param password: the user password
    :type password: String
    :param dateNaissance: the user's date of birth
    :type dateNaissance: DateTime
    :param numEtu: the ID of the user
    :type numEtu: String
    :return: the User created
    :rtype: User
    """
    user = User(numEtu=numEtu, nom=nom, prenom=prenom, administrateur=administrateur, password=password,
                dateNaissance=dateNaissance)
    state = db.session.add(user)
    db.session.commit()
    return user


def create_ent(nomEnt, priorite, blackList, desc, code_ape, effectifs, num_siren, num_siret, telephone, adresse):
    """
    create a compagny and put it in the database

    :param nomEnt: the name of the compagny
    :type nomEnt: String
    :param priorite: if the company has a priority
    :type priorite: boolean
    :param blackList: if the company is blacklisted
    :type blackList: boolean
    :param desc: the description of the compagny
    :type desc: String
    :param code_ape: the code APE of the compagny
    :type code_ape: String
    :param effectifs: the company's membership
    :type effectifs: int
    :param num_siren: the company's siren number
    :type num_siren: String
    :param num_siret: the company's siret number
    :type num_siret: String
    :return: no return
    :rtype: None
    """
    db.session.add(
        Ent(nomEnt=nomEnt, priorite=priorite, blackList=blackList, desc=desc, code_ape=code_ape, effectifs=effectifs, num_siren=num_siret, num_siret=num_siret, telephone=telephone, adresse=adresse))
    db.session.commit()


def get_all_ent():
    """
    get all compagny

    :return: a list of all compagny
    :rtype: list
    """
    return Ent.query.all()


def get_all_user():
    """
    get all user

    :return: a list of all user
    :rtype: list
    """
    return User.query.all()


def get_all_comp():
    """
    get all skill

    :return: a list of all skill
    :rtype: list
    """
    return Competence.query.all()


def reset_nb_proposition_ent(id_ent):
    """
    reset to 0 the number of times a company has been proposed

    :param id_ent: the ID of a compagny
    :type id_ent: int
    :return: no return
    :rtype: None
    """
    ent = get_Ent(id_ent)
    ent.nb_proposition = 0
    db.session.commit()


def reset_nb_proposition_all_ent():
    """
    reset to 0 the number of times all companies have been proposed

    :return: no return
    :rtype: None
    """
    compagnies = get_all_ent()
    for compagny in compagnies:
        compagny.nb_proposition = 0
    db.session.commit()


def get_avg_comp_by_etu():
    """
    get the average number of comp own by users

    :return: int
    :rtype:
    """
    cpt = 0
    data = db.session.query(User._competences).all()
    users = db.session.query(User.numEtu).all()
    if data and len(data) > 0:
        cpt = sum(map(lambda x: x[0], data))
        return format(cpt / len(users), '1.0f')
    return 0


def get_avg_comp_by_ent():
#     """

#     :return:
#     :rtype:
#     """
#     cpt = 0
#     data = db.session.query(Ent._competences).all()
#     ents = db.session.query(Ent.idEnt).all()
#     if data and len(data) > 0:
#         cpt = sum(map(lambda x: x[0], data))
#         return cpt / len(ents)
    return 0
#     # return format(cpt/len(ents), '1.0f')


def get_avg_like():
    """
    

    :return:
    :rtype:
    """
    nblike = 0;
    users = db.session.query(User.numEtu).all()
    for user in users:
        nblike +=len(get_like(user[0]))
    return format(nblike/len(users), '1.0f')

def get_avg_dislike():
    """
    

    :return:
    :rtype:
    """
    nbdislike = 0;
    users = db.session.query(User.numEtu).all()
    for user in users:
        nbdislike +=len(get_dislike(user[0]))
    return format(nbdislike/len(users), '1.0f')


def blackList_ent(ent_id, value):
    """
    set the blacklist value of the compagny to true or false

    :param ent_id: the ID of a compagny
    :type ent_id: int
    :param value: true or false
    :type value: boolean
    :return: if the modification was successful
    :rtype: boolean
    """
    state = db.session.query(Ent).filter(Ent.idEnt == ent_id).update({Ent.blackList: value})
    db.session.commit()
    ent = db.session.query(Ent).filter(Ent.idEnt == ent_id).first()
    print(ent.blackList)
    return state


def setAdmin(numEtu, value):
    """
    set the admin value of the User to true or false

    :param numEtu: the ID of the user
    :type numEtu: String
    :param value: true or false
    :type value: boolean
    :return: if the modification was successful
    :rtype: boolean
    """
    state = db.session.query(User).filter(User.numEtu == numEtu).update({User.administrateur: value})
    db.session.commit()
    return state


@login_manager.user_loader
def load_user(numEtu):
    """
    equivalent to get_User but needed to login_manager

    :param numEtu: the ID of the user
    :type numEtu: String
    :return: the user identified by numEtu
    :rtype: User
    """
    return User.query.get(numEtu)

def get_status_company(numEtu):
    """
    return all companies
    """
    res = []
    propositions = select(proposition).where((proposition.c.num_etu == numEtu))
    rs = db.engine.connect().execute(propositions)
    for row in rs:
        if row not in res:
            res.append(row)
    return res


def set_like(id_etu, idEnt, like):
    """

    :param id_etu:
    :type id_etu:
    :param idEnt:
    :type idEnt:
    :param like:
    :type like:
    :return:
    :rtype:
    """
    up = update(proposition).where((proposition.c.num_etu == id_etu) & (proposition.c.idEnt == idEnt)).values(like=like)
    db.engine.connect().execute(up)
    return 0

def get_like(id_etu):
    res = []
    companies = get_status_company(id_etu)
    for comp in companies:
        if comp[2] == True:
            comp_string = get_Ent(comp[1])
            res.append(comp_string)
    return res

def get_dislike(id_etu):
    res = []
    companies = get_status_company(id_etu)
    for comp in companies:
        if comp[2] == False:
            comp_string = get_Ent(comp[1])
            res.append(comp_string)
    return res

def purpose_ent_to_user(numEtu, id_ent):
    """
    add the company to user //under devllopement TODO//
    
    :param numEtu:
    :type numEtu:
    :param id_ent:
    :type id_ent:
    :return:
    :rtype:
    """
    u = get_User(numEtu)
    ent = get_Ent(id_ent)
    u._entreprises.append(ent)
    db.session.commit()


def add_log():
    """
    add an entrie to the Log
    :return: return if the log has been added
    :rtype: boolean
    """
    currDate = datetime.datetime.now()
    log = Log(logTime=currDate)
    state = db.session.add(log)
    db.session.commit()
    return state

def stat_like_Ent():
    """
    return ape group by number of like and order by likes

    :return: the list of ape number with its number of like order by number of like
    :rtype: list
    """
    res = []
    likes = select(proposition.c.idEnt, func.count(proposition.c.like)).group_by(proposition.c.idEnt).order_by(proposition.c.like)
    rs = db.engine.connect().execute(likes)
    for row in rs:
        if row not in res:
            res.append(row)

    ape = select(Ent.idEnt, Ent.code_ape) 
    rs2 = db.engine.connect().execute(ape)
    res2 = []

    for row in rs2:
        if row not in res2:
            res2.append(row)
    
    res3 = []
    for idEnt, numAPE in res2:
        for idEnt2, nblike in res:
            if idEnt == idEnt2 and numAPE not in res:
                res3.append((numAPE, nblike))
            elif idEnt == idEnt2:
                for i in range(len(res3)):
                    if numAPE == res[i][0]:
                        res[i][1] = res[i][1]+nblike
    print('APE :'+str(res3))
    return res3

def stat_APE_Ent():
    """
    return ape group by number of companies and order by number of companies

    :return: the list of ape number with is number of companies order by number of companies
    :rtype: list
    """
    res = []
    likes = select(Ent.code_ape, func.count(Ent.idEnt)).group_by(Ent.code_ape).order_by(Ent.idEnt)
    rs = db.engine.connect().execute(likes)
    for row in rs:
        if row not in res:
            res.append(row)
    print('APE :'+str(res))
    return res

# def stat_Log():
#     """
#     """
#     res= []
#     logs= select(Log.idLog,Log.logTime)
#     rs = db.engine.connect().execute(logs)
#     for row in rs:
#         if row not in res:
#             res.append(row)
#     return res