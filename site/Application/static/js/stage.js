let profiles = document.querySelectorAll('.profile');
var decline = document.getElementById('decline');
var match = document.getElementById('match');

//** Recovery of company names **//
let list_company = [];
for(let i = 0; i<document.body.getElementsByClassName("profile__name").length; i++){
    const nameCompany = document.getElementsByClassName("profile__name")[i].childNodes[0].nodeValue;
    list_company.push(nameCompany);
}
const maxAngle = 42;
const smooth = 0.3;
const threshold = 42;
const thresholdMatch = 150;
profiles.forEach(setupDragAndDrop);

//** drag and drop card function **//

function setupDragAndDrop(profile) {
  const hammertime = new Hammer(profile);
  var moveOutWidth = document.body.clientWidth * 1.5;

  hammertime.on('pan', function (e) {
    profile.classList.remove('profile--back');
    let posX = e.deltaX;
    let posY = Math.max(0, Math.abs(posX * smooth) - 42);
    let angle = Math.min(Math.abs(e.deltaX * smooth / 100), 1) * maxAngle;
    if (e.deltaX < 0) {
      angle *= -1;
    }

    profile.style.transform = `translateX(${posX}px) translateY(${posY}px) rotate(${angle}deg)`;
    profile.classList.remove('profile--matching');
    profile.classList.remove('profile--nexting');
    if (posX > thresholdMatch) {
      profile.classList.add('profile--matching');
    } else if (posX < -thresholdMatch) {
      profile.classList.add('profile--nexting');
    }

    if (e.isFinal) {
      profile.style.transform = ``;
      if (posX > thresholdMatch) {
        profile.classList.add('profile--back');
        profile.style.transform = 'translate(' + moveOutWidth + 'px, -100px) rotate(-10deg)';
        profile.classList.add('removed');
        EntAccept();
      } else if (posX < -thresholdMatch) {
        profile.classList.add('profile--back');
        profile.style.transform = 'translate(-' + moveOutWidth + 'px, -100px) rotate(10deg)';
        profile.classList.add('removed');
        EntDecline();
      } else {
        profile.classList.add('profile--back');
      }
    }
  });
}
  
//** button card function **//

  function createButtonListener(match) {
    return function (event) {
      var cards = document.querySelectorAll('.profile:not(.removed)');
      var moveOutWidth = document.body.clientWidth * 1.5;

      if (!cards.length) return false;
  
      var card = cards[cards.length -1];
      
      card.classList.add('profile--back');
      card.classList.add('removed');
  
      if (match) {
        card.style.transform = 'translate(' + moveOutWidth + 'px, -100px) rotate(-10deg)';
        EntAccept();
      } else {
        card.style.transform = 'translate(-' + moveOutWidth + 'px, -100px) rotate(10deg)';
        EntDecline();
      }
      event.preventDefault();
  };
  }

var declineListener = createButtonListener(false);
var matchListener = createButtonListener(true);

decline.addEventListener('click', declineListener);
match.addEventListener('click', matchListener);

function EntDecline(){
  if(list_company.length > 0){
    const name = list_company.pop();
    console.log(name)
    $.ajax({
      url:"/recupStages",
      data:{'nameEnt':name,'likeEnt':false},
      type:'POST'
    });
  }
};

function EntAccept(){
  if(list_company.length > 0){
    const name = list_company.pop();
    console.log(name)
    $.ajax({
      url:"/recupStages",
      data:{'nameEnt':name,'likeEnt':true},
      type:'POST'
    });
  }
};