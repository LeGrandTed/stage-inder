/* Code By Webdevtrick ( https://webdevtrick.com ) */
// include("https://cdn.jsdelivr.net/npm/chart.js");
(function(document) {
	'use strict';

	var TableFilter = (function(Arr) {

		var _input;

		function _onInputEvent(e) {
			_input = e.target;
			var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
			Arr.forEach.call(tables, function(table) {
				Arr.forEach.call(table.tBodies, function(tbody) {
					Arr.forEach.call(tbody.rows, _filter);
				});
			});
		}

		function _filter(row) {
			var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
		}

		return {
			init: function() {
				var inputs = document.getElementsByClassName('light-table-filter');
				Arr.forEach.call(inputs, function(input) {
					input.oninput = _onInputEvent;
				});
			}
		};
	})(Array.prototype);

	document.addEventListener('readystatechange', function() {
		if (document.readyState === 'complete') {
			TableFilter.init();
		}
	});

})(document);

function deleteEnt(id) {
	$.ajax({
		url: "Admin/DeleteEnt/"+id,
		type: "POST",
		success:function () {
			deleteRow("rowEnt"+id)
		},
		error:function(err) {
			alert("something went wrong")
		}
	});
}

function deleteEtu(id) {
	$.ajax({
		url: "Admin/DeleteEtu/"+id,
		type: "POST",
		success:function () {
			deleteRow("rowEtu"+id)
		},
		error:function() {
			console.log("non")
			alert("Cet utilisateur est un administrateur, vous ne pouvez pas supprimer les administrateurs");
		}
	});
}

function deleteComp(id) {
	$.ajax({
		url: "Admin/DeleteComp/"+id,
		type: "POST",
		success:function () {
			deleteRow("rowComp"+id)
		},
		error:function(err) {
			alert("something went wrong")
		}
	});
}
  

function deleteRow(id) {
	let row = document.getElementById(id);
	let parent = row.parentNode;
	parent.removeChild(row);
}


function sauverEnt() {
	let table = document.getElementById("table-ent")
	let length = table.rows['length'];
	let BlackListData = new Array();
	for (let i = 1; i < length; i++) {
		BlackListData.push([table.rows[i].cells[2].children[0].id,table.rows[i].cells[2].children[0].checked]);
	}

	console.log(BlackListData);
	$.ajax({
		url: "Admin/SauverEnt",
		type: "POST",
		data: {BlackListData : JSON.stringify(BlackListData)},
		success:function () {
			alert("Everything went well");
		},
		error:function(err) {
			alert("Something went wrong");
		}
	});
}

function sauverEtu() {
	let table = document.getElementById("table-etu")
	let length = table.rows['length'];
	let AdminData = new Array();
	for (let i = 1; i < length; i++) {
		AdminData.push([table.rows[i].cells[3].children[0].id,table.rows[i].cells[3].children[0].checked]);
	}
	
	console.log(AdminData);
	$.ajax({
		url: "Admin/SauverEtu",
		type: "POST",
		data: {AdminData : JSON.stringify(AdminData)},
		success:function () {
			alert("Everything went well");
		},
		error:function(err) {
			alert("Something went wrong");
		}
	});
}


let labelsDoughnut = []
let datasDoughnut = []

function getDataLikes() {
	const xmlhttp = new XMLHttpRequest();
	xmlhttp.onload = function() {
		const myObj = JSON.parse(this.responseText);
  	for(x in myObj.data){
  		console.log(myObj.data[x][0]);
  		labelsDoughnut.push(myObj.data[x][0])
  		datasDoughnut.push(myObj.data[x][1])
  	}
	}	
xmlhttp.open("GET", "Admin/getStatsLikes");
xmlhttp.send();
}
getDataLikes();

const dataDoughnut = {
  labels: labelsDoughnut,
  datasets: [{
    data: datasDoughnut,
    backgroundColor: [
      'rgb(255, 99, 132)',
      'rgb(255, 159, 64)',
      'rgb(255, 205, 86)',
      'rgb(75, 192, 192)',
      'rgb(54, 162, 235)'
    ],
    borderWidth: 1
  }]
};

const configDoughnut = {
  type: 'doughnut',
  data: dataDoughnut,
  options: {
    plugins: {
    	title: {
    		display: true,
    		text: 'Répartition des like sur les APE'
    	}
    }
  },
};
const myChart = new Chart(
    document.getElementById('myChart'),
    configDoughnut,

);


let labelsPie = []
let datasPie = []

function getDataAPE() {
	const xmlhttp = new XMLHttpRequest();
	xmlhttp.onload = function() {
		const myObj = JSON.parse(this.responseText);
  	for(x in myObj.data){
  		console.log(myObj.data[x][0]);
  		labelsPie.push(myObj.data[x][0])
  		datasPie.push(myObj.data[x][1])
  	}
	}

xmlhttp.open("GET", "Admin/getStatsAPE");
xmlhttp.send();
}
getDataAPE();
const dataPie = {
  labels: labelsPie,
  datasets: [{
    label: 'Présence APE dans la base',
    data: datasPie,
    backgroundColor: [
      'rgb(255, 99, 132)',
      'rgb(255, 159, 64)',
      'rgb(255, 205, 86)',
      'rgb(75, 192, 192)',
      'rgb(54, 162, 235)'
    ],
    hoverOffset: 5
  }]
};


const configPie = {
	options: {
		plugins: {
			title: {
				display: true,
				text: 'Présence APE dans la base'
			}
		}
  },
  type: 'pie',
  data: dataPie,
};


const myChart2 = new Chart(
  	document.getElementById('myChart2'),
    configPie
);

