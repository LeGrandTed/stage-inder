from .app import app, db
from .InsertionBD import jeu_dessai
import click
from .BDclass import *
import json


@app.cli.command()
def loaddb():
    """Creates the tables and populates them with data."""
    jeu_dessai()

    entreprises = json.load(open('Application/scrap/data.json'))

    for e in entreprises:
        for ee in entreprises[e]:
            ent = Ent(nomEnt=e, priorite=False, blackList=False, desc=ee[" Code APE"][7:], code_ape=ee[" Code APE"][0:5], effectifs=ee[" Effectif"].split(" ")[0], num_siren=ee[" Siret"][:9], num_siret=ee[" Siret"].replace(" ", ""), telephone=ee[" Téléphone"][:19], adresse=ee[" Adresse"], image=ee[" Lien Image"].replace(" ", "&")) #, desc=entreprises[e])
            db.session.add(ent)
    db.session.commit()


@app.cli.command()
@click.argument("nom")
@click.argument("prenom")
@click.argument("password")
@click.argument("numetu")
def newuser(nom, prenom, password, numetu):
    """Adds a new user"""
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = User(Nom=nom, Prenom=prenom, Password=m.hexdigest(), NumEtu=numetu, Administrateur=True)
    db.session.add(u)
    db.session.commit()


@app.cli.command()
def showUser():
    """Show all user in databases"""
    print(User.query.all())


@app.cli.command()
def showEnt():
    """Show all user in databases"""
    print(Ent.query.all())


@app.cli.command()
@click.argument("numetu")
def getUser(numetu):
    """Show a user with is student number"""
    print(User.query.get(numetu))


@app.cli.command()
@click.argument("numetu")
@click.argument("password")
def passwd(numetu, password):
    """change the password for a user"""
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = get_User(numetu)
    u.Password = m.hexdigest()
    db.session.commit()


@app.cli.command()
@click.argument("id1")
# @click.argument("id2")
def test(id1):
    print(get_User(id1))


@app.cli.command()
def reset_all_proposition():
    reset_nb_proposition_all_ent()


@app.cli.command()
@click.argument("id1")
def reset_proposition(id1):
    reset_nb_proposition_ent(id1)
