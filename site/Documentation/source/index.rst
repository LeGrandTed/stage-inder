Bienvenue sur la Documentation de Stage'Inder!
==============================================

Main functions
==============
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules
   Scraping

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
