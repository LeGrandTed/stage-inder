Application package
===================

Submodules
----------

Application.BDclass module
--------------------------

.. automodule:: Application.BDclass
   :members:
   :undoc-members:
   :show-inheritance:

Application.InsertionBD module
------------------------------

.. automodule:: Application.InsertionBD
   :members:
   :undoc-members:
   :show-inheritance:

Application.app module
----------------------

.. automodule:: Application.app
   :members:
   :undoc-members:
   :show-inheritance:

Application.commands module
---------------------------

.. automodule:: Application.commands
   :members:
   :undoc-members:
   :show-inheritance:

Application.views module
------------------------

.. automodule:: Application.views
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: Application
   :members:
   :undoc-members:
   :show-inheritance:
